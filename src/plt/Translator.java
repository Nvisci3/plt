package plt;

public class Translator{
	
	public static final  String NIL = "nil";
	
	private String phrase;
	private int dashIsPresence = 0;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}
	
	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		if(startsWithVowel()) {
			if(phrase.endsWith("y")) {
				return phrase + "nay";
		}else if (endWithVowel()) {
			return phrase + "yay";
		}else if (!endWithVowel()) {
			return phrase + "ay";
		}
		}
		
		return NIL;
	}
	
	
	private boolean startsWithVowel() {
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u");	    
		}
	
	
	private boolean endWithVowel() {
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u");	    
		}
	
	public String translateAWordStartingWithASingleOrMoreConsonants() {
		
		int firstCaracterIsConsonant = 0;
		int secondCaracterIsConsonant = 0;
		char temp;
		
		for(int i = 0; i < 2; i++) {
			temp = phrase.charAt(i);
			if(i == 0 && (temp != 'a') && (temp != 'e') && (temp != 'i') && (temp != 'o') && (temp != 'u')) {
				firstCaracterIsConsonant = 1;
			}else if(i == 1 && (temp != 'a') && (temp != 'e') && (temp != 'i') && (temp != 'o') && (temp != 'u')) {
				secondCaracterIsConsonant = 1;
			}
		}
		
		if(firstCaracterIsConsonant == 1 && secondCaracterIsConsonant == 0) {		
			return phrase.substring(1) + phrase.charAt(0) + "ay";
		}else if(firstCaracterIsConsonant == 1 && secondCaracterIsConsonant == 1) {
			return phrase.substring(2) + phrase.charAt(0) + phrase.charAt(1) + "ay";
		}
		    return phrase;
	}
	
	public String translateAPhraseContainingMoreWords() {
		
		String[] words = null;
		String word = "";
		int space = 0;
		int dash = 0;
		
		if(phrase.contains(" ")) {
			words = phrase.split(" ");
			space = 1;
		}else if(phrase.contains("-")) {
			words = phrase.split("-");
			dash = 1;
		}
		for(int i = 0;i < words.length; i++) {
			phrase = words[i];
			if(!startsWithVowel()) {
			    word += translateAWordStartingWithASingleOrMoreConsonants();
			}else {
				word += translate();
			}
			if(dash == 1 && i < words.length-1) {
				word += "-";
			}else if(space == 1 && dashIsPresence == 1 && i<words.length-1) {
				        word += "-";
			      }else if(space == 1 && i < words.length-1) {
				           word += " ";
			}
		}
		
		
		return word;
	}
	
    public String translateAPhraseContainingPunctuations() {
	
	
	
	String phraseCopy = phrase;
	String subStringPhrase = "";
	String stringFinal = "";
	char[] charSearch = {'.',',',';',':','?','!','\'','(',')'};
	int index = 0;
	int dash = 0;
	
	for(int a = 0;a<phrase.length();a++) {
		char charDash = phrase.charAt(a);
		if(charDash == '-') {
			index = a;
			dashIsPresence = 1;	
			dash = 1;
			subStringPhrase = phraseCopy.substring(0, index) + " " + phraseCopy.substring(index+1, phraseCopy.length());
			
		}
	}
	
		
		if((dash != 1)) {
		String phraseCopyNoDash = phrase;
		String phraseClean = phrase.replaceAll("\\p{Punct}", "");
		
		
        phrase = phraseClean;
		
		phraseClean = translateAPhraseContainingMoreWords();
		
		for(int i = 0;i<phraseCopyNoDash.length();i++) {
			char chr = phraseCopyNoDash.charAt(i);
			for(int j = 0;j<charSearch.length;j++) {
				if(charSearch[j] == chr) {
					phraseClean += chr;
				}
			}
		}return phraseClean;
		
		}else if(dash == 1) {
			
			String phraseCopyWithDash = phrase;
			
			phrase = subStringPhrase.replaceAll("\\p{Punct}", "");
			
			
			
			stringFinal = translateAPhraseContainingMoreWords();
			
			for(int i = 0;i<phraseCopyWithDash.length();i++) {
				char chr = phraseCopyWithDash.charAt(i);
				for(int j = 0;j<charSearch.length;j++) {
					if(charSearch[j] == chr) {
						stringFinal += chr;
					}
				}
			}
			
			return stringFinal;
			
			
		}else return NIL;
      
    }
    
}