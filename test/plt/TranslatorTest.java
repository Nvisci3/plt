package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String  inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String  inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		String  inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String  inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithVovel() {
		String  inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithConsonant() {
		String  inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithASingleConsonant() {
		String  inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translateAWordStartingWithASingleOrMoreConsonants());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonants() {
		String  inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translateAWordStartingWithASingleOrMoreConsonants());
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedByADash() {
		String  inputPhrase = "hello-known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay-ownknay", translator.translateAPhraseContainingMoreWords());
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedByASpace() {
		String  inputPhrase = "hello known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ownknay", translator.translateAPhraseContainingMoreWords());
	}
	
	
	@Test
	public void testTranslationAPhraseContainingPunctuations() {
		String  inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translateAPhraseContainingPunctuations());
	}
	
	@Test
	public void testTranslationAPhraseContainingPunctuationsCompositeWordVowel() {
		String  inputPhrase = "are-known(";
		Translator translator = new Translator(inputPhrase);
		assertEquals("areyay-ownknay(", translator.translateAPhraseContainingPunctuations());
	}
	
	@Test
	public void testTranslationAPhraseContainingPunctuationsCompositeWordConsonant() {
		String  inputPhrase = "hello-known?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay-ownknay?", translator.translateAPhraseContainingPunctuations());
	}
	
}
